package com.gmail.avereshchaga.testrocketsdk

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import chat.rocket.common.model.UserStatus
import chat.rocket.core.model.Email
import chat.rocket.core.model.Myself
import io.reactivex.Single.just
import java.util.function.Consumer

class MainActivity : AppCompatActivity() {

    private lateinit var tvMessage: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tvMessage = findViewById(R.id.tv_message)

//        main()
            getMeInfoByRx()
    }

}
