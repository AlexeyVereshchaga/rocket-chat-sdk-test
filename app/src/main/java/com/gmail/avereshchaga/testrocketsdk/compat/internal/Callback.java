package com.gmail.avereshchaga.testrocketsdk.compat.internal;

import chat.rocket.common.RocketChatException;


public interface Callback<T> {
    void onSuccess(T data);

    void onError(RocketChatException error);
}
